import warnings, numpy as np, pandas as pd, matplotlib.pyplot as plt, os, multiprocessing, sys, errno
from collections import Iterable
import matplotlib.pyplot as plt
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from sklearn.model_selection import train_test_split
from sklearn import metrics, preprocessing
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier, BaggingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from xgboost import XGBClassifier
from sklearn.neural_network import MLPClassifier
import winsound

space_hp = []
space = []
dataset = {}

def classifiers(params):
    global dataset 
    
    np.random.RandomState(7)
    classifier = params['type']
    test = params['test']
    params.pop('type')
    params.pop('test')
    clf=classifier(**params)
    clf.fit(dataset['X_train'], dataset['y_train'])

    if test:
        X_test = dataset['X_test']
        y_test = dataset['y_test']
        pred_prob = clf.predict_proba(X_test)
        acc = metrics.accuracy_score(y_test, clf.predict(X_test))
        f1 = metrics.f1_score(y_test, clf.predict(X_test), average='macro')
        auc = metrics.roc_auc_score(y_test, clf.predict(X_test), average='macro')

        id = [i for i in range(1,len(pred_prob)+1)]
        pred_prob = np.insert(pred_prob, 0, id, axis=1)
        pred_prob = pred_prob[(-pred_prob[:,2]).argsort()]
        pred_prob = np.around(pred_prob, 6)

        return acc, f1, auc, pred_prob
    
    else:
        return -metrics.roc_auc_score(dataset['y_dev'], clf.predict(dataset['X_dev']), average='macro')

def run_files_parallel():
    global space_hp, space

    accs = []
    aucs = []
    f1s = []
    pred_probs = []

    for sp in space_hp:
        sp['test'] = False
        typ = sp['type']

        trials = Trials()
        best = fmin(fn=classifiers, space=sp, algo=tpe.suggest, max_evals=50, trials=trials, verbose=1, rstate=np.random.RandomState(7))

        b = {}
        for key in best.keys():
            for s in space:
                if s['type'] == typ:
                    if type(best[key]) == int or type(best[key]) == bool:
                        b[key] = s[key][best[key]]
                        if 'random_state' in s.keys():
                            b['random_state'] = s['random_state']
                        if 'n_jobs' in s.keys():
                            b['n_jobs'] = s['n_jobs']
                        if 'probability' in s.keys():
                            b['probability'] = s['probability']
                    elif type(best[key]) == float:
                        b[key] = best[key]

        b['test'] = True
        b['type'] = typ
        
        acc, f1, auc, pred_prob = classifiers(b)
        # print('AUC: ', auc)
        # print('trials:')
        # for trial in trials.trials[:2]:
        #     print(trial)

        accs.append(acc)
        f1s.append(f1)
        aucs.append(auc)
        pred_probs.append(pred_prob)

    return accs, f1s, aucs, pred_probs

def main():
    global dataset, space_hp, space

    file = sys.argv[1]
    np.set_printoptions(formatter={'float_kind':'{:f}'.format})

    print(file)

    # data = pd.read_csv(file, header=None).values
    # X_train, X_test, y_train, y_test = train_test_split(data[:,1:], (data[:,0]=='anomaly').astype(int), test_size=0.3)
    # X_train, X_dev, y_train, y_dev = train_test_split(X_train, y_train, test_size=0.2)
    # file = file.split('/')[-1][:-6]
    # dataset = {'X_train': X_train, 'X_test': X_test, 'X_dev': X_dev, 'y_dev': y_dev, 'y_train': y_train, 'y_test':y_test}

    path1 = './train_test_split/'

    dataset = {}
    dataset['X_train'] = pd.read_csv(path1 + file + '_X_train.csv', header=None).values
    dataset['y_train'] = pd.read_csv(path1 + file + '_y_train.csv', header=None).values
    dataset['X_test'] = pd.read_csv(path1 + file + '_X_test.csv', header=None).values
    dataset['y_test'] = pd.read_csv(path1 + file + '_y_test.csv', header=None).values
    dataset['X_dev'] = pd.read_csv(path1 + file + '_X_dev.csv', header=None).values
    dataset['y_dev'] = pd.read_csv(path1 + file + '_y_dev.csv', header=None).values

    min_max_scaler = preprocessing.MinMaxScaler()
    for key in dataset.keys():
        dataset[key] = min_max_scaler.fit_transform(dataset[key])

    random_state = 7
    np.random.sample(7)
    n_jobs = -1
    ks = [15, 10, 5]

    #pass tuples for when float or hp.uniform along with desired search scale
    #pass list when acutal values need to be sampled
    space = [
            {
                'type': RandomForestClassifier, 
                'random_state': random_state,
                'n_jobs': n_jobs,
                'bootstrap': [True, False],
    #             'oob_score': [True, False],
                'max_depth': list(range(10, 101))+[None],
                'max_features': ['auto', 'sqrt', 'log2', None],
                'min_samples_leaf': range(1, 15),
                'min_samples_split': range(2, 15),
                'n_estimators': range(1,2001),
                'criterion': ['gini', 'entropy'],
            },
            {
                'type': XGBClassifier,
                'class_weight': [None, 'balanced'],
                'boosting_type': ['gbdt', 'dart', 'goss'],
    #             'subsample': (0.5, 1, hp.uniform),
                'num_leaves': (30, 150, 1, hp.quniform),
                'learning_rate': (np.log(0.01), np.log(0.2), hp.loguniform),
                'subsample_for_bin': (20000, 300000, 20000, hp.quniform),
                'min_child_samples': (20, 500, 5, hp.quniform),
                'reg_alpha': (0.0, 1.0, hp.uniform),
                'reg_lambda': (0.0, 1.0, hp.uniform),
                'colsample_bytree': (0.6, 1.0, hp.uniform),
            },
            {
                'type': BaggingClassifier,
                'random_state': random_state,
                'n_jobs': n_jobs,
                'n_estimators': range(1, 2001),
                'bootstrap': [True, False],
    #             'oob_score': [True, False],
                'bootstrap_features': [True, False],
                'max_samples': (0.01, 1, hp.uniform),
                'max_features': (0.05, 1, hp.uniform),
            },
            {
                'type': LogisticRegression,
                'random_state': random_state,
                'n_jobs': n_jobs,
                'penalty': ['l1', 'l2'],
    #             'solver': ['liblinear', 'saga', 'newton-cg', 'sag', 'lbfgs'],
                'C' : (0, 10, hp.uniform),
            },
            {
                'type': GaussianNB
            },
            {
                'type': AdaBoostClassifier,
                'random_state': random_state,
                'n_estimators': range(1, 2001),
                'learning_rate': (0, 1, hp.loguniform),
                'algorithm': ['SAMME', 'SAMME.R'],
            },
            {
                'type': KNeighborsClassifier,
                'n_jobs': n_jobs,
                'n_neighbors': range(1, 50),
                'weights': ['uniform', 'distance'],
                'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute'],
                'leaf_size': range(1, 100),
                'metric': ['euclidean', 'manhattan', 'chebyshev', 'minkowski']
            },
            {
                'type': MLPClassifier,
                'random_state': random_state,
                'max_iter': range(10, 500),
                'early_stopping': [True, False],
                'hidden_layer_sizes': [(50,50,50), (50,100,50), (100,), (100,600,1)],
                'activation': ['identity', 'tanh', 'relu', 'logistic'],
                'solver': ['sgd', 'adam', 'lbfgs'],
                'alpha': (0.0001, 0.9, hp.uniform),
                'learning_rate': ['constant','adaptive'],
                'batch_size': range(10, 200),
                'learning_rate': ['constant', 'invscaling', 'adaptive'],
            },
            {
                'type': SVC,
                'random_state': random_state,
                'probability': True,
                'cache_size': 20480,
                'C' : (0, 4, hp.uniform),
                'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
                'degree': range(1, 5),
                'gamma': ['auto', 'scale'],
                'class_weight': [None, 'balanced'],
                'C' : (0, 4, hp.uniform),
            },
    ]

    for i in space:
        temp_dict = {}
        for key in i.keys():
            if type(i[key]) == tuple:
                temp_dict[key] = i[key][-1](key, *i[key][:-1])
            elif isinstance(i[key], Iterable):
                temp_dict[key] = hp.choice(key, i[key])
            else:
                temp_dict[key] = i[key]
        space_hp.append(temp_dict)

    columns = np.array(['RF', 'XGB', 'BagC', 'LogR', 'GNB', 'ABC', 'KNN', 'MLP', 'SVM', 'Dataset Name'])
    auc_df = pd.DataFrame(columns=columns)
    acc_df = pd.DataFrame(columns=columns)
    f1_df = pd.DataFrame(columns=columns)

    res = []
    
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        res.append(run_files_parallel())

    for accs, f1s, aucs, predict_prob in res:

        aucs.append(file)
        auc_df.loc[-1] = aucs
        auc_df.index = auc_df.index + 1
        auc_df = auc_df.sort_index()

        accs.append(file)
        acc_df.loc[-1] = accs
        acc_df.index = acc_df.index + 1
        acc_df = acc_df.sort_index()

        f1s.append(file)
        f1_df.loc[-1] = f1s
        f1_df.index = f1_df.index + 1
        f1_df = f1_df.sort_index()

    if sys.argv[2]:
        os.chdir(sys.argv[2])
        try:
            os.mkdir(file)
        except OSError as exc:
            if exc.errno == errno.EEXIST:
                pass
        os.chdir(file)
        auc_df.to_csv(file+'_auc.csv', sep=',')
        acc_df.to_csv(file+'_acc.csv', sep=',')
        f1_df.to_csv(file+'_f1.csv', sep=',')

        cols = columns[columns!='Dataset Name']
        for k in ks:
            top_k_df = pd.DataFrame(columns=columns)
            top_ks = []
            for i in range(len(predict_prob)):
                predict_prob_df = pd.DataFrame(predict_prob[i], columns=['idx', 'nominal', 'anomaly'])
                predict_prob_df.to_csv(file+'_'+str(cols[i])+'_probs.csv', sep=',')
                
                top_k = predict_prob[i][:int(len(predict_prob[i])*k*0.01)]
                cnt = 0
                for i in top_k:
                    if int(dataset['y_test'][int(i[0] - 1)]) == int(max(i[1], i[2])==i[2]) : cnt += 1
                top_ks.append(cnt)
            
            top_ks.append(file)
            top_k_df.loc[-1] = top_ks
            top_k_df.index = top_k_df.index + 1
            top_k_df = top_k_df.sort_index()
            top_k_df.to_csv(file+'_'+str(k)+'_topk.csv', sep=',')

if __name__ == '__main__': 
    main()
    frequency = 2500  # Set Frequency To 2500 Hertz
    duration = 1000  # Set Duration To 1000 ms == 1 second
    winsound.Beep(frequency, duration)
