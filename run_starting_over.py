import os, numpy as np, pandas as pd
from sklearn.model_selection import train_test_split

input_str = '''#!/bin/bash
#PBS -V
#PBS -N bl-def-pred-file_name_here
#PBS -q doppa
#PBS -l nodes=1:ppn=4,walltime=100:00:00,mem=8gb
#PBS -e /home/sadidass/bb_python/error-file_name_here.txt
#PBS -o /home/sadidass/bb_python/out-file_name_here.txt
#PBS -M sriyandass.adidass@wsu.edu
#PBS -m abe

module load python/3.6.1
#pip install --upgrade pip
pip install --user virtualenv
./.local/bin/virtualenv --version
./.local/bin/virtualenv -p /share/apps/python3.6.1/bin/python py_venv_file_name_here
source py_venv_file_name_here/bin/activate
pip install -r /home/sadidass/bb_python/requirements.txt
pip install xgboost
pip install hyperopt

cd /home/sadidass/bb_python
python hyperparameter_tune.py file_name_here ./hp_outputs
'''

files = []
path = './ad_examples/datasets/anomaly/'
for _, _, filenames in os.walk(path):
    for filename in filenames:
        if "_1.csv" in filename and filename != 'toy2_1.csv':
            file = filename[:-6] + '/fullsamples/' + filename
            data = pd.read_csv(path + file, header=None).values
            X_train, X_test, y_train, y_test = train_test_split(data[:,1:], (data[:,0]=='anomaly').astype(int), test_size=0.3, random_state=7)
            X_train, X_dev, y_train, y_dev = train_test_split(X_train, y_train, test_size=0.285714, random_state=7)
            file = file.split('/')[-1][:-6]
            path1 = './train_test_split/'
            np.savetxt(path1 + file + '_X_train.csv', X_train, delimiter=",")
            np.savetxt(path1 + file +'_y_train.csv', y_train, delimiter=",")
            np.savetxt(path1 + file +'_X_test.csv', X_test, delimiter=",")
            np.savetxt(path1 + file +'_y_test.csv', y_test, delimiter=",")
            np.savetxt(path1 + file +'_X_dev.csv', X_dev, delimiter=",")
            np.savetxt(path1 + file +'_y_dev.csv', y_dev, delimiter=",")
            input_str_f = input_str.replace('file_name_here', file)
            with open('run_ml_code.pbs', 'w') as f:
                f.write(input_str_f)
            os.system('cat run_ml_code.pbs')