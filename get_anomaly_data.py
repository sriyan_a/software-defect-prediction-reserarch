import numpy as np, pandas as pd, os
import multiprocessing

def output(item):
    print('bash ./aad.sh ' + item[0] + ' ' + str(item[1]) + ' 10 0.03 '+ str(item[2]) +' 1 0 0 512 0 1 1')
    # with open('./ad_examples/run_code.pbs', o) as f:
    #     f.write(str)

all_files = []
num_queries = {}

def get_data(path):
    f = open(path, "r")
    return f.readlines()

def write_data(file, req_data, attr_data):
    # all_files.append(file)
    # num_queries[file] = attr_data.count('anomaly')
    # path =  "./ad_examples/ad_examples/datasets/anomaly/" + file + "/fullsamples/"
    # if not os.path.exists(path):
    #     os.makedirs(path)

    print(file)
    print(req_data.count('\n'))

    # with open(path + file + "_1.csv", 'w') as w:
    #     w.write(req_data)
    
    # with open(path + file + "_orig_labels.csv", 'w') as w:
    #     w.write(attr_data)

def get_promise_data(file, test_string1, test_string2):
    req_data = ''
    attr_data = ''
    path = "datasets/" + file
    lines = get_data(path)

    for line in lines:
        line = line.strip()
        if (('@' not in line) and ('%' not in line) and ('?' not in line) and (line != '')):
            line = line.split(',')
            if line[-1] == test_string1:
                st = 'anomaly'
            elif line[-1] == test_string2:
                st = 'nominal'
            attr_data += st + '\n'
            if file == "eclipse34_debug.arff":
                req_data += ','.join([st] + line[:-2]) + '\n'
            else:
                req_data += ','.join([st] + line[:-1]) + '\n'

    write_data(file[:-5], req_data, attr_data)

def get_usi_data(file):
    req_data = ''
    attr_data = ''
    path = "datasets/" + file
    lines = get_data(path)
    
    for line in lines[1:]:
        line = line.strip()
        line = line.split(' ; ')[:-4]
        if int(line[-1]) > 0:
            st = 'anomaly'
        else:
            st = 'nominal'
        attr_data += st + ',' + line[-1] + '\n'
        req_data += ','.join([st] + line[1:-1]) + '\n'

    write_data(file[:-4], req_data, attr_data)

def get_wroc_data(file):
    req_data = ''
    attr_data = ''
    path = "datasets/" + file
    lines = get_data(path)

    for line in lines[1:]:
        line = line.strip()
        line = line.split(';')
        if int(line[-1]) > 0:
            st = 'anomaly'
        else:
            st = 'nominal'
        attr_data += st + ',' + line[-1] + '\n'
        line = ','.join([st] + line[3:-1]) + '\n'
        req_data += line

    write_data(file[:-4], req_data, attr_data)

def num_of_queries(file):
    return list(np.genfromtxt(file)).count(1)

def main():

    # promise_files = ["cm1.arff", "jm1.arff", "kc1.arff", "pc1.arff"]
    # for file in promise_files:
    #     get_promise_data(file, 'true', 'false')

    # file = "kc1-class-level-defectiveornot.arff"
    # get_promise_data(file, '_TRUE', 'FALSE')
    
    # file = "kc1-class-level-top5percentDF.arff"
    # get_promise_data(file, 'DEF', 'NODEF')

    # kim_files = ["eclipse34_swt.arff", "eclipse34_debug.arff"]
    # for file in kim_files:
    #     get_promise_data(file, 'TRUE', 'FALSE')

    # usi_files = ["Eclipse-JDT-CK-11-other.csv", "Eclipse-PDE-CK-11-other.csv", "Mylyn-CK-11-other.csv"]
    # for file in usi_files:
    #     get_usi_data(file)

    # wroc_files = ["prop-1.csv", "prop-2.csv", "prop-3.csv", "prop-4.csv", "prop-5.csv", "tomcat_v6.0.csv", "camel_v1.6.csv", "xalan_v2.7.csv"]
    # for file in wroc_files:
    #     get_wroc_data(file)

    path = './train_test_split'

    parallel_struct = []

    for dirs, folders, files in os.walk(path):
        for file in files:
            if '_y_train.csv' in file:
                parallel_struct.append([file.replace('_y_train.csv', ''), num_of_queries(os.path.join(dirs, file)), 7])
                parallel_struct.append([file.replace('_y_train.csv', ''), num_of_queries(os.path.join(dirs, file)), 11])
    
    # for file in all_files:
    #     parallel_struct.append([file, num_queries[file], 7])
    #     parallel_struct.append([file, num_queries[file], 11])

    # # flag = 'w'
    # os.chdir('./ad_examples')
    for item in parallel_struct:
        # output(flag, 'aad.sh ' + item[0] + ' ' + str(item[1]) + ' 10 0.03 '+ str(item[2]) +' 1 0 0 512 0 1 1\n')
        output(item)
    #     # if flag == 'w':
    #     #     flag = 'a'

    # # os.chdir('./ad_examples')
    # # with multiprocessing.Pool(2) as p:
    # #     p.map(output, parallel_struct)
    
    # # os.system('python ./ad_examples/base-line-models.py')
    # # os.system('scp -r /Users/Colossus/Projects/software-defect-prediction-reserarch/ad_examples sadidass@aeolus.wsu.edu:~/bb_python/')

if __name__ == "__main__": main()